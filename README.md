# druxt-x3

## Env settings

* Copy .env-example to .env
* Provide a valid backend, below is the current default, a version of our cms with adaptations for this project
* BASE_URL=https://decoupled-playground.djurensratt.se

## Try out with gitpod


[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/emdahlstrom/druxt-x3)

## Build Setup

```bash
# install dependencies
$ yarn install

# set up .env
$ cp .env-example .env

# serve with hot reload at localhost:3000
$ yarn dev

# generate static project (for testing or prod)
$ yarn generate

# serve static project locally - requires prior generation
$ yarn serve
```


## Gotchas

**Patches:** We sometimes use [patch-package](https://www.npmjs.com/package/patch-package) to use pre-release versions of druxt packages since they are under active development and we are in close contact with Stuart, the main developer of druxt. It automatically runs the patches in ./patches on a postinstall hook.

**Redirects:** Images that are exposed as their own fields are easy to rewrite on the frontend, but images embedded in a text either require a url redirect or processing the text to rewrite the url.

In netlify.toml you will find redirects that handle these images, but in the longer term we likely want to do more context aware manipulation of size and quality.

# Nuxt docs

For detailed explanation on how things work, check out the [documentation](https://nuxtjs.org).
## Special Directories

You can create the following extra directories, some of which have special behaviors. Only `pages` is required; you can delete them if you don't want to use their functionality.

### `assets`

The assets directory contains your uncompiled assets such as Stylus or Sass files, images, or fonts.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/assets).

### `components`

The components directory contains your Vue.js components. Components make up the different parts of your page and can be reused and imported into your pages, layouts and even other components.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/components).

### `layouts`

Layouts are a great help when you want to change the look and feel of your Nuxt app, whether you want to include a sidebar or have distinct layouts for mobile and desktop.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/layouts).


### `pages`

This directory contains your application views and routes. Nuxt will read all the `*.vue` files inside this directory and setup Vue Router automatically.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/get-started/routing).

### `plugins`

The plugins directory contains JavaScript plugins that you want to run before instantiating the root Vue.js Application. This is the place to add Vue plugins and to inject functions or constants. Every time you need to use `Vue.use()`, you should create a file in `plugins/` and add its path to plugins in `nuxt.config.js`.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/plugins).

### `static`

This directory contains your static files. Each file inside this directory is mapped to `/`.

Example: `/static/robots.txt` is mapped as `/robots.txt`.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/static).

### `store`

This directory contains your Vuex store files. Creating a file in this directory automatically activates Vuex.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/store).
