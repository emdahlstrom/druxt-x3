module.exports = {
  content: [],
  theme: {
    extend: {
      fontFamily: {
        'sans': ['BaileySans', 'Helvetica', 'Arial', 'Nimbus Sans L', 'sans-serif'],
        'serif': ['Sabon', 'Georgia', 'Times New Roman', 'serif'],
      },
    },
    colors: {
      black: {
        DEFAULT: "#1d1d1b",
      },
      white: {
        DEFAULT: "#fff",
      },
      pink: {
        lighter: "#fef2f8",
        light: "#ec62ad",
        DEFAULT: "#e6007e",
      },
      purple: {
        DEFAULT: "#5f4198",
      },
      grey: {
        lighter: "#f4f4f4",
        light: "#d8d8d8",
        DEFAULT: "#a8a8a7",
        dark: "#646363",
        darker: "#3c3c3c",
        700: '3f3c3c'
      },
      blue: {
        lighter: "#f2fbfd",
        200: "#4cb1ce",
        light: "#33c3db",
        DEFAULT: "#00b4d2",
        dark: "#008fa7",
        darker: "#2d8fa7",
      },
      red: {
        DEFAULT: "#e00034",
      },
      green: {
        light: "#2ecc40",
        200: "#76b852",
        DEFAULT: "#9dba64",
        dark: "#24a274",
        darker: "#218661",
      },
      yellow: {
        DEFAULT: "#efd826",
        500: "#f6af8c",
      },
    },
  },
  plugins: [
    require('@tailwindcss/forms'),
    require('@tailwindcss/typography'),
  ],
}
