const baseUrl = process.env.BASE_URL

export default {
  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',

  publicRuntimeConfig: {
    baseUrl: process.env.BASE_URL || 'https://decoupled-playground.djurensratt.se'
  },

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'druxt-x3',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  generate: {
    routes: [
      '/',
      '/views' // This route contains a list of links to feed the crawler
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    "~assets/css/global.css",
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    '@nuxt/typescript-build',
    '@nuxtjs/tailwindcss',
    // Custom Search API Lunr module.
    [
      '~/modules/search-api-lunr',
      {
        server: 'druxt',
        index: 'default',
      },
    ],
    'druxt-site',
    'druxt-layout-paragraphs',
    '@nuxt/image',
    '@nuxtjs/pwa',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
// Nuxt.js Lunr.
    [
      '@nuxtjs/lunr-module',
      {
        fields: [
          'title',
          'body',
          // 'field_ingredients',
          // 'field_recipe_instruction',
        ],
      },
    ],
  ],

  image: {
    cloudinary: {
      baseURL: 'https://res.cloudinary.com/klandestino-ab/image/fetch/'
    },
    domains: ['djurensratt.se', 'decoupled-playground.djurensratt.se', 'cloudinary.com'],
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  },

  druxt: {
    baseUrl,
    site: { theme: 'client' },
    proxy: { files: true,  api: true },
    debug: false,
    router: { pages: true, wildcard: true },
    views: {
      bundleFilter: true,
      fields: ['title'],
    }
  },

  proxy: {
    '/en/jsonapi': baseUrl,
    '/sv/jsonapi': baseUrl,
  },

  telemetry: false,

}
